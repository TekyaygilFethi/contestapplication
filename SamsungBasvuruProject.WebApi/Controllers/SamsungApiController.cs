﻿using SamsungBasvuruProject.Business;
using SamsungBasvuruProject.Data;
using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Results;

namespace SamsungBasvuruProject.WebApi.Controllers
{
    public class SamsungApiController : ApiController
    {
        Repository<Application> _applicationRepository = new Repository<Application>();
        Repository<User> _userRepository = new Repository<User>();

        #region IMEI KONTROL
        public bool IMEICompare(string _IMEI)
        {
            List<string> _IMEIList = new List<string>();//dbdeki tabloyu çağır.Select ile sadece IMEI numarlarını al
            _IMEIList.Add("123457789876543");
            _IMEIList.Add("123123123123123");
            _IMEIList.Add("123412341234123");
            _IMEIList.Add("123451234512345");
            _IMEIList.Add("123432123432123");
            _IMEIList.Add(_IMEI);
            if (_IMEIList.Where(w => w.Contains(_IMEI)).Any())
                return true;

            else
                return false;
            //return InternalServerError("IMEI number could not be found!");
        }
        #endregion
        
        #region Resim işlemleri
        public byte[] ImageToDatabase(string _base64Image)
        {
            byte[] _imageByted = Convert.FromBase64String(_base64Image);
            return _imageByted;
        }

        //[HttpPost]
        //public IHttpActionResult DatabaseToImage(byte[] _bytedImage)
        //{
        //    ImageByteOperations _ibo = new ImageByteOperations();
        //    var _image = _ibo.byteArrayToImage(_bytedImage);
            
        //    return Ok(_image);
        //}
        #endregion

        #region Başvuru al
        [HttpPost]
        public async Task<IHttpActionResult> AddApplication(ApplicationFormData _appFormData)
        {
            var _requestResult = IMEICompare(_appFormData.IMEINumber);

            if (_requestResult)
            {
                Application _newApplication = new Application();
                _newApplication.Name = _appFormData.Name;
                _newApplication.Surname = _appFormData.Surname;
                _newApplication.MobileNumber = _appFormData.MobileNumber;
                _newApplication.IMEINumber = _appFormData.IMEINumber;
                _newApplication.Image = ImageToDatabase(_appFormData.TempByte64Image);
                
                await _applicationRepository.Add(_newApplication);
                try
                {
                    await _applicationRepository.Save();
                }
                catch(Exception ex)
                {
                    var e = ex.Message;
                    return Content(HttpStatusCode.BadRequest, ex.Message);
                }
                return Content(HttpStatusCode.Accepted, "Başvurunuz işleme alınmıştır!");
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, "Bu telefon modeli ile işlem yapamazsınız!");
            }
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> SMSValidateControl(string _imei, string _validateCodeByUser)
        {
            var _application = await _applicationRepository.SingleGetBy(b => b.IMEINumber == _imei);

            if (_application.SMSCode == _validateCodeByUser)
            {
                _application.IsSmsValidated = true;
                _applicationRepository.Save();
                return Content(HttpStatusCode.Accepted, "Şifre doğrudur!");
            }
            else
            {
                return Content(HttpStatusCode.BadRequest, "Şifre yanlıştır");
            }
        }

        #endregion
    }
}
