﻿using SamsungBasvuruProject.Business;
using SamsungBasvuruProject.Data;
using SamsungBasvuruProject.Data.TableClasses;
using SamsungBasvuruProject.WebApi;
using SamsungBasvuruProject.WebApi.FormDatas;
using SamsungBasvuruProject.WebApi.Filters;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SamsungBasvuruProject.Controllers
{
    public class SamsungController : AsyncController
    {
        Repository<User> _userRepository = new Repository<User>();
        Repository<Application> _applicationRepository = new Repository<Application>();

        public ActionResult Compare()
        {
            return View();
        }

        public ActionResult Login() // ÖNEMLİ:session ekle metodu sil!!!!!!!!!
        {
            return View(new User());
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(User _user)
        {
            if (ModelState.IsValid)
            {
                var _checkValue = await checkAccount(_user);
                if (_checkValue)
                {
                    Session["User"] = _user;

                    FormsAuthentication.SetAuthCookie(_user.Username, false);

                    return RedirectToAction("ListApplications");
                }
                else
                {
                    @ViewBag.Message = "Kullanıcı Adı veya Şifrenizi tekrar kontrol ediniz!";
                    @ViewBag.Status = "danger";
                    return View();
                }
            }
            else
                return View();
        }

        public async Task<bool> checkAccount(User _user)
        {
            var _userCheck = await _userRepository.SingleGetBy(B => B.Username == _user.Username);


            if (_userCheck != null)
            {
                HashSaltClass hsc = new HashSaltClass();
                var _saltValue = _user.PasswordSalt;

                return await hsc.CheckPassword(_user.Username, _user.Password);
            }
            else
                return false;
        }

        [ActFilter, ResFilter, AuthFilter]
        public async Task<ActionResult> ListApplications()
        {
            List<ApplicationImageFormData> _appFormDataList = new List<ApplicationImageFormData>();
            //ImageByteOperations _ibo = new ImageByteOperations();
            var _applicationRepositories = await _applicationRepository.GetAll();

            foreach (var _appItem in _applicationRepositories.ToList())
            {
                ApplicationImageFormData _appFormData = new ApplicationImageFormData();
                _appFormData.Application = _appItem;
                _appFormData.Image = "data:image/png;base64," + Convert.ToBase64String(_appItem.Image);
                CheckApproveTypes(ref _appFormData);

                _appFormDataList.Add(_appFormData);
            }
            int ApprovedCount = _appFormDataList.Where(w => w.ApproveType == "Onaylanmış").Count();
            int UnApprovedCount = _appFormDataList.Where(w => w.ApproveType == "Henüz onaylanmamış").Count();
            int DeniedCount = _appFormDataList.Where(w => w.ApproveType == "Reddedilmiş").Count();

            @ViewBag.ApprovedCount = ApprovedCount;
            @ViewBag.UnApproved = UnApprovedCount;
            @ViewBag.DeniedCount = DeniedCount;

            return View(_appFormDataList.ToList());
        }


        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();


            return RedirectToAction("Login");
        }

        public void CheckApproveTypes(ref ApplicationImageFormData _appImageForm)
        {
            if (_appImageForm.Application.IsRejected == false && _appImageForm.Application.IsApproved == false)
            {
                _appImageForm.ApproveType = "Henüz onaylanmamış";
            }
            if (_appImageForm.Application.IsRejected == true && _appImageForm.Application.IsApproved == false)
            {
                _appImageForm.ApproveType = "Reddedilmiş";
            }
            if (_appImageForm.Application.IsRejected == false && _appImageForm.Application.IsApproved == true)
            {
                _appImageForm.ApproveType = "Onaylanmış";
            }

        }

        public ActionResult ReturnImage(byte[] imageData)
        {
            return new FileStreamResult(new System.IO.MemoryStream(imageData), "image/jpeg");
        }

        public async Task<ActionResult> ApplicationDetails(Guid ID)
        {
            var _application = await _applicationRepository.SingleGetBy(b => b.ID == ID);
            ApplicationImageFormData _appFormData = new ApplicationImageFormData();
            _appFormData.Application = _application;
            _appFormData.Image = "data:image/png;base64," + Convert.ToBase64String(_application.Image);
            @ViewBag.Username = _appFormData.Application.NameSurname + "'in Başvurusu";

            return View(_appFormData);
        }

        [HttpPost]
        public async Task<JsonResult> AcceptApplication(string _IMEINumber, string _isApproved, string _cityName, string _type)
        {
            var _approvedApplication = await _applicationRepository.SingleGetBy(b => b.IMEINumber == _IMEINumber);

            if (_type == "Accept")
            {
                _approvedApplication.IsApproved = true;
            }
            else
            {
                _approvedApplication.IsApproved = bool.Parse(_isApproved);
            }


            if (_cityName != null)
            {
                _approvedApplication.City = _cityName;
            }

            string Status;
            string Message;
            try
            {
                await _applicationRepository.Save();
                if (_type == "Accept")
                {
                    Status = "success";
                    Message = "Başvuru başarıyla onaylandı!";
                }
                else
                {
                    Status = "success";
                    Message = "Güncelleme başarıyla yapıldı!";
                }
            }
            catch (Exception ex)
            {
                if (_type == "Accept")
                {
                    Status = "danger";
                    Message = "Başvuru sırasında bir hata oluştu!";
                }
                else
                {
                    Status = "danger";
                    Message = "Güncelleme sırasında bir hata oluştu!";
                }
            }

            List<string> list = new List<string>();
            list.Add(Status);
            list.Add(Message);
            return Json(list);
        }

        [HttpPost]
        public async Task<JsonResult> DenyApplication(string _IMEINumber)
        {
            var _deniedApplication = await _applicationRepository.SingleGetBy(b => b.IMEINumber == _IMEINumber);
            _deniedApplication.IsApproved = false;
            _deniedApplication.IsRejected = true;

            string Status;
            string Message;
            try
            {
                await _applicationRepository.Save();

                Status = "success";
                Message = "Başvuru başarıyla reddedildi!";
            }
            catch (Exception ex)
            {
                Status = "danger";
                Message = "Başvuru reddetme sırasında bir hata oluştu!";
            }

            List<string> list = new List<string>();
            list.Add(Status);
            list.Add(Message);
            return Json(list);
        }

        public ActionResult CreateAccount()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreateAccount(User _user)
        {
            HashSaltClass hsc = new HashSaltClass();

            User _newUser = new User();
            
            _newUser.Username = _user.Username;
            _newUser.PasswordSalt = hsc.GetSalt();
            _newUser.Password = hsc.GetHashSaltPassword(_user.Password, _newUser.PasswordSalt);

            await _userRepository.Add(_newUser);

            int _saveResult = 0;
            try
            {
                _saveResult = await _userRepository.Save();
                if (_saveResult > 0)
                {
                    @ViewBag.Status = "success";
                    @ViewBag.Message = "Kullanıcı başarıyla eklendi!";
                }
                else
                {
                    @ViewBag.Status = "info";
                    @ViewBag.Message = "Hiç bir kayıt değişmedi!";
                }


            }
            catch (Exception ex)
            {
                @ViewBag.Status = "danger";

                var _innerEx = ex.InnerException;
                Exception _tempInnerEx = _innerEx;

                if (_tempInnerEx != null)
                {
                    while (_tempInnerEx != null)
                    {
                        _tempInnerEx = _innerEx.InnerException;

                        if (_tempInnerEx == null)
                        {
                            @ViewBag.Message = _innerEx.Message;
                        }
                        _innerEx = _innerEx.InnerException;
                    }
                }

            }
            return View();
        }

        public ActionResult CreateApplication()
        {
            return View(new SamsungBasvuruProject.Data.TableClasses.Application());
        }


    }
}