﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SamsungBasvuruProject.WebApi
{
    public class ApplicationFormData
    {

        public string Name { get; set; }

        public string Surname { get; set; }

        public string MobileNumber{get;set;}
        public string IMEINumber { get; set; }

        public string TempByte64Image { get; set; }
    }
}