﻿using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SamsungBasvuruProject.WebApi.FormDatas
{
    public class ApplicationImageFormData
    {
        public ApplicationImageFormData()
        {
            ApproveType = "Henüz onaylanmamış";
        }
        public Application Application { get; set; }

        public string Image { get; set; }

        public string ApproveType { get; set; }
    }
}