﻿using SamsungBasvuruProject.Business;
using SamsungBasvuruProject.Data;
using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SamsungBasvuruProject.WebApi.Filters
{
    public class ActFilter : FilterAttribute ,IActionFilter
    {
        Repository<User> _userRepository = new Repository<User>();
        Repository<Log> _logRepository = new Repository<Log>();

        public void OnActionExecuted(ActionExecutedContext filterContext)
        {
            Log _newLog = new Log();
            _newLog.ActionName = filterContext.ActionDescriptor.ActionName;
            _newLog.ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            _newLog.Date = DateTime.Now;
            _newLog.Username = (filterContext.HttpContext.Session["User"] as User).Username;
            _newLog.Info = "OnActionExecuting";

             _logRepository.Add(_newLog);
             _logRepository.Save();

            //var user=_userRepository.GetBy(b=>b.Username==Session)
        }

        public  void OnActionExecuting(ActionExecutingContext filterContext)
        {
            Log _newLog = new Log();
            _newLog.ActionName = filterContext.ActionDescriptor.ActionName;
            _newLog.ControllerName = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName;
            _newLog.Date = DateTime.Now;
            _newLog.Username = (filterContext.HttpContext.Session["User"] as User).Username;
            _newLog.Info = "OnActionExecuted";

             _logRepository.Add(_newLog);
             _logRepository.Save();
        }
    }
}