﻿using SamsungBasvuruProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SamsungBasvuruProject.WebApi.Filters
{
    public class AuthFilter : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            if(filterContext.HttpContext.Session["User"] == null)
            {
                filterContext.Result = new RedirectResult("../Samsung/Login");
            }
        }
    }
}