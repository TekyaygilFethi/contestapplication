﻿using SamsungBasvuruProject.Business;
using SamsungBasvuruProject.Data;
using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SamsungBasvuruProject.WebApi.Filters
{
    public class ResFilter : FilterAttribute, IResultFilter
    {
        Repository<User> _userRepository = new Repository<User>();
        Repository<Log> _logRepository = new Repository<Log>();

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            Log _newLog = new Log();
            _newLog.ActionName = filterContext.RouteData.Values["action"].ToString();
            _newLog.ControllerName = filterContext.RouteData.Values["controller"].ToString();
            _newLog.Date = DateTime.Now;
            _newLog.Username = (filterContext.HttpContext.Session["User"] as User).Username;
            _newLog.Info = "OnResultExecuting";

             //SaveDb(_logRepository, _newLog);
              _logRepository.Add(_newLog);
             _logRepository.Save();
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
            Log _newLog = new Log();
            _newLog.ActionName = filterContext.RouteData.Values["action"].ToString();
            _newLog.ControllerName = filterContext.RouteData.Values["controller"].ToString();
            _newLog.Date = DateTime.Now;
            _newLog.Username = (filterContext.HttpContext.Session["User"] as User).Username;
            _newLog.Info = "OnResultExecuting";

            //SaveDb(_logRepository, _newLog);
             _logRepository.Add(_newLog);
             _logRepository.Save();
        }

        public async Task<int> SaveDb(Repository<Log> rep,Log _log)
        {
            await rep.Add(_log);
            return await _logRepository.Save();
        }
    }
}