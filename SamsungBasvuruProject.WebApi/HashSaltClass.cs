﻿using SamsungBasvuruProject.Business;
using SamsungBasvuruProject.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Helpers;

namespace SamsungBasvuruProject.WebApi
{
    public class HashSaltClass
    {
        Repository<User> _userRepository = new Repository<User>();
        public string GetSalt()
        {
            return Crypto.GenerateSalt();
        }
        
        public string GetHashSaltPassword(string _password,string _salt)
        {
            var _passwordLast=_password+_salt;

            return Crypto.HashPassword(_passwordLast);
        }

        public async Task<bool> CheckPassword(string _username,string _password)
        {
            var _user = await _userRepository.SingleGetBy(b => b.Username == _username);

            var _userEnteredPassword = _password + _user.PasswordSalt;

            bool IsMatch=Crypto.VerifyHashedPassword(_user.Password, _userEnteredPassword);

            if (IsMatch)
            {
                return true;
            }
            else
                return false;
        }
    }
}