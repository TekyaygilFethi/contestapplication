﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamsungBasvuruProject.Data.TableClasses
{
    public class Application
    {
        public Application()
        {
            IsApproved = false;
            IsDeleted = false;
            IsRejected = false;
            IsSmsValidated = false;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [Required]
        [DisplayName("İsim")]
        public string Name { get; set; }

        [DisplayName("Soyisim")]
        [Required]
        public string Surname { get; set; }

        [DisplayName("Telefon Numarası")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^(05)([0-9]){9}$")]
        public string MobileNumber{get;set;}

        [RegularExpression("([0-9]){15}$")]
        [StringLength(15,MinimumLength=15,ErrorMessage="IMEI numarası 15 haneli olmalıdır!")]
        [DisplayName("IMEI Numarası")]
        [Required]
        [Index(IsUnique=true)]
        [Editable(false)]
        public string IMEINumber { get; set; }

        [DisplayName("Resim")]
        [Required]
        public byte[] Image { get; set; }

        [DisplayName("Şehir")]
        public string City { get; set; }

        [DisplayName("Onaylandı mı?")]
        [Required]
        public bool IsApproved { get; set; }

        [DisplayName("Silindi mi?")]
        [Required]
        public bool IsDeleted { get; set; }

        [DisplayName("Reddedildi mi?")]
        public bool IsRejected { get; set; }

        [DisplayName("SMS Validasyonu Yapıldı mı?")]
        public bool IsSmsValidated { get; set; }

        [DisplayName("SMS Gönderildi mi?")]
        public bool IsSMSSuccessful { get; set; }

        [StringLength(4,MinimumLength=4,ErrorMessage="SMS Kodu 4 Haneli Olmalıdır!")]
        public string SMSCode { get; set; }

        [NotMapped]
        public string NameSurname
        {
            get
            {
                return Name + " " + Surname;
            }
        }

        [NotMapped]
        public string TempByte64Image { get; set; }
    }
}
