﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamsungBasvuruProject.Data.TableClasses
{
    public class Coupon
    {

        public Coupon()
        {
            IsUsed = false;
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [DisplayName("IMEI Numarası")]
        [RegularExpression("([0-9]){15}$")]
        [StringLength(15, MinimumLength = 15, ErrorMessage = "IMEI numarası 15 haneli olmalıdır!")]
        [Index(IsUnique=true)]
        [Required]
        public string IMEI { get; set; }

        [DisplayName("Kupon Kodu")]
        [Required]
        public string CouponCode { get; set; }

        [DisplayName("Kullanıldı mı?")]
        [Required]
        public bool IsUsed { get; set; }
    }
}
