﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamsungBasvuruProject.Data.TableClasses
{
    public class Log
    {
        [Key,DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [Required,DisplayName("Tarih")]
        public DateTime Date { get; set; }

        [Required,DisplayName("Kullanıcı Adı")]
        public string Username { get; set; }

        [Required,DisplayName("Action")]
        public string ActionName { get; set; }

        [Required,DisplayName("Controller")]
        public string ControllerName { get; set; }

        [DisplayName("Bilgi")]
        public string Info { get; set; }
    }
}
