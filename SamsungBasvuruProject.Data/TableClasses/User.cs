﻿using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamsungBasvuruProject.Data
{
    public class User
    {
        public User()
        {
            IsActive = true;
            Logs = new List<Log>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }

        [DisplayName("Kullanıcı Adı")]
        [Index("UsernameIndex",IsUnique=true)]
        [MaxLength(50)]
        public string Username { get; set; }

        [DisplayName("Şifre")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DisplayName("Aktif mi?")]
        public bool IsActive { get; set; }

        public string PasswordSalt { get; set; }

        public virtual List<Log> Logs { get; set; }
    }
}
