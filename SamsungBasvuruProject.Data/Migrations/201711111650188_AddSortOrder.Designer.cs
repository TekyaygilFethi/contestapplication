// <auto-generated />
namespace SamsungBasvuruProject.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddSortOrder : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddSortOrder));
        
        string IMigrationMetadata.Id
        {
            get { return "201711111650188_AddSortOrder"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
