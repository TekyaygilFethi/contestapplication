﻿using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamsungBasvuruProject.Data
{
    public class SamsungProjectDb : DbContext
    {
        public SamsungProjectDb(): base("SamsungBasvuruProjectDb"){ }

        public DbSet<User> Users { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<Coupon> Coupons { get; set; }
        public DbSet<Log> Logs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().ToTable("UserTable");
            modelBuilder.Entity<Application>().ToTable("ApplicationTable");
            modelBuilder.Entity<Coupon>().ToTable("CouponTable");
            modelBuilder.Entity<Log>().ToTable("LogTable");

            Database.SetInitializer<SamsungProjectDb>(null);
            base.OnModelCreating(modelBuilder);
        }
    }
}
