﻿using SamsungBasvuruProject.Data;
using SamsungBasvuruProject.Data.TableClasses;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SamsungBasvuruProject.Business
{
    public class Repository<T> where T : class
    {
        SamsungProjectDb db = new SamsungProjectDb();

        public Repository()
        {

        }

        public async Task Add(T _item)
        {
            await Task.Factory.StartNew(() => db.Set<T>().Add(_item));
        }

        public async Task Update(T _newitem)
        {
            await Task.Factory.StartNew(() => db.Entry(_newitem).State = EntityState.Modified);
        }

        public async Task Delete(T _deletedItem)
        {
            await Task.Factory.StartNew(() =>
            {
                if (_deletedItem.GetType().ToString() == "User")
                {
                    (_deletedItem as User).IsActive = false;
                }
                if (_deletedItem.GetType().ToString() == "Application")
                {
                    (_deletedItem as Application).IsDeleted = true;
                }
                if (_deletedItem.GetType().ToString() == "Coupon")
                {
                    (_deletedItem as Coupon).IsUsed = true;
                }
            });
        }

        public async Task<T> Get(Guid ID)
        {
            var ResultSet = Task.Factory.StartNew(() =>
            {
                return db.Set<T>().Find(ID);
            });
            return await ResultSet;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            var ResultSet = Task.Factory.StartNew(() =>
            {
                return db.Set<T>().AsEnumerable();
            });
            return await ResultSet;
        }

        public async Task<IEnumerable<T>> GetBy(Func<T, bool> predicate)
        {
            var TSet = Task.Factory.StartNew(() =>
            {
                return db.Set<T>().Where(predicate);
            });
            return await TSet;
        }

        public async Task<T> SingleGetBy(Func<T, bool> predicate)
        {
            var TSet = Task.Factory.StartNew(() =>
            {
                return db.Set<T>().Where(predicate).SingleOrDefault();
            });
            return await TSet;
        }

        public async Task<DbSet<T>> GetDbSet()
        {
            return await Task.Factory.StartNew(()=>db.Set<T>());
        }

        public async Task<int> Save()
        {
            return await db.SaveChangesAsync();
        }

        public void DisposeContext()
        {
             db.Dispose();
        }
    }
}
